package com.bumptech.glide.provider;

import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.MultiClassKey;
import ohos.utils.LightweightMap;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Maintains a cache of Model + Resource class to a set of registered resource classes that are
 * subclasses of the resource class that can be decoded from the model class.
 */
public class ModelToResourceClassCache {
    private final AtomicReference<MultiClassKey> resourceClassKeyRef = new AtomicReference<>();
    private final LightweightMap<MultiClassKey, List<Class<?>>> registeredResourceClassCache =
            new LightweightMap<>();

    public List<Class<?>> get(
            @NotNull Class<?> modelClass,
            @NotNull Class<?> resourceClass,
            @NotNull Class<?> transcodeClass) {

        MultiClassKey key = resourceClassKeyRef.getAndSet(null);
        if (key == null) {
            key = new MultiClassKey(modelClass, resourceClass, transcodeClass);
        } else {
            key.set(modelClass, resourceClass, transcodeClass);
        }
        final List<Class<?>> result;
        synchronized (registeredResourceClassCache) {
            result = registeredResourceClassCache.get(key);
        }
        resourceClassKeyRef.set(key);
        return result;
    }

    public void put(
            @NotNull Class<?> modelClass,
            @NotNull Class<?> resourceClass,
            @NotNull Class<?> transcodeClass,
            @NotNull List<Class<?>> resourceClasses) {
        synchronized (registeredResourceClassCache) {
            registeredResourceClassCache.put(
                    new MultiClassKey(modelClass, resourceClass, transcodeClass), resourceClasses);
        }
    }

    public void clear() {
        synchronized (registeredResourceClassCache) {
            registeredResourceClassCache.clear();
        }
    }
}
