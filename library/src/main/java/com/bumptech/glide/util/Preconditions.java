package com.bumptech.glide.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

/** Contains common assertions. */
public final class Preconditions {

  private Preconditions() {
    // Utility class.
  }

  public static void checkArgument(boolean expression, @NotNull String message) {
    if (!expression) {
      throw new IllegalArgumentException(message);
    }
  }

  @NotNull
  public static <T> T checkNotNull( @Nullable T arg) {
    return checkNotNull(arg, "Argument must not be null");
  }

  @NotNull
  public static <T> T checkNotNull(@Nullable T arg,  String message) {
    if (arg == null) {
      throw new NullPointerException(message);
    }
    return arg;
  }

  @NotNull
  public static String checkNotEmpty( @Nullable String string) {
    if (Util.isEmpty(string)) {
      throw new IllegalArgumentException("Must not be null or empty");
    }
    return string;
  }

  @NotNull
  public static <T extends Collection<Y>, Y> T checkNotEmpty(@NotNull T collection) {
    if (collection.isEmpty()) {
      throw new IllegalArgumentException("Must not be empty.");
    }
    return collection;
  }
}
