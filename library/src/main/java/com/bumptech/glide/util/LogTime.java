package com.bumptech.glide.util;

import ohos.miscservices.timeutility.Time;

/** A class for logging elapsed real time in millis. */
public final class LogTime {
  private static final double MILLIS_MULTIPLIER =  1d / Math.pow(10, 6);

  private LogTime() {
    // Utility class.
  }

  /**
   * Returns the current time in either millis or nanos depending on the api level to be used with
   * {@link #getElapsedMillis(long)}.
   * @return long
   */
  public static long getLogTime() {
      return Time.getRealTimeNs();
  }

  /**
   * Returns the time elapsed since the given logTime in millis.
   *
   * @param logTime The start time of the event.
   * @return double
   */
  public static double getElapsedMillis(long logTime) {
    return (getLogTime() - logTime) * MILLIS_MULTIPLIER;
  }
}
