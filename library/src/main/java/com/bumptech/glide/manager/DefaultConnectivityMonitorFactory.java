package com.bumptech.glide.manager;

import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;

/**
 * A factory class that produces a functional {@link com.bumptech.glide.manager.ConnectivityMonitor}
 * if the application has the {@code ohos.security.SystemPermission.GET_NETWORK_INFO} permission and a no-op
 * non functional {@link com.bumptech.glide.manager.ConnectivityMonitor} if the app does not have
 * the required permission.
 */
public class DefaultConnectivityMonitorFactory implements ConnectivityMonitorFactory {
  private static final String TAG = "ConnectivityMonitor";
  private static final String NETWORK_PERMISSION = "ohos.security.SystemPermission.GET_NETWORK_INFO";

  @Override
  public ConnectivityMonitor build(
       @NotNull Context context,
       @NotNull ConnectivityMonitor.ConnectivityListener listener) {

    boolean hasPermission = false; // TODO:
    if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
      LogUtil.info(
          TAG,
          hasPermission
              ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor"
              : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
    }
    return hasPermission
        ? new DefaultConnectivityMonitor(context, listener)
        : new NullConnectivityMonitor();
  }
}
