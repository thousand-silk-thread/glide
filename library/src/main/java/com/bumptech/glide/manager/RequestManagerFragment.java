package com.bumptech.glide.manager;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Synthetic;
import ohos.aafwk.ability.ILifecycle;
import ohos.aafwk.ability.ILifecycleObserver;
import ohos.aafwk.ability.Lifecycle;
import ohos.aafwk.ability.LifecycleStateObserver;
import ohos.aafwk.content.Intent;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class RequestManagerFragment {
    private static final String TAG = "RMFragment";
    private Lifecycle realLifecycle;
    private ILifecycleObserver realObserver;
    private final ActivityFragmentLifecycle lifecycle;
    private final RequestManagerTreeNode requestManagerTreeNode =
            new FragmentRequestManagerTreeNode();

    private  RequestManager requestManager;

    public RequestManagerFragment(Lifecycle realLifecycle) {
        this(new ActivityFragmentLifecycle(),realLifecycle);
    }

    public RequestManagerFragment(ActivityFragmentLifecycle lifecycle, Lifecycle realLifecycle) {
        this.lifecycle = lifecycle;
        this.realLifecycle = realLifecycle;

        this.realObserver = new LifecycleStateObserver() {
            @Override
            public void onStateChanged(Lifecycle.Event event, Intent intent) {
                if(event == Lifecycle.Event.ON_START || event == Lifecycle.Event.ON_FOREGROUND){
                    lifecycle.onStart();
                } else if(event == Lifecycle.Event.ON_BACKGROUND){
                  lifecycle.onStop();
                }else if(event == Lifecycle.Event.ON_STOP){
                    lifecycle.onStop();
                    realLifecycle.removeObserver(realObserver);
                }
            }
        };
        this.realLifecycle.addObserver(realObserver);
    }

    /**
     * Sets the current
     *
     * @param requestManager The request manager to use
     */
    public void setRequestManager(RequestManager requestManager){
        this.requestManager = requestManager;
    }

    ActivityFragmentLifecycle getGlideLifecycle(){
        return lifecycle;
    }


    /**
     * Returns the current
     * @return RequestManager
     */
    public RequestManager getRequestManager(){
        return requestManager;
    }

    /**
     * Returns the RequestManagerTreeNode for this fragment
     *
     * @return RequestManagerTreeNode
     */
    public RequestManagerTreeNode getRequestManagerTreeNode(){
        return  requestManagerTreeNode;
    }

    Set<RequestManagerFragment> getDescendantRequestManagerFragments(){
        Set<RequestManagerFragment> descendants = new HashSet<>();
        return Collections.unmodifiableSet(descendants);
    }

    private class FragmentRequestManagerTreeNode implements RequestManagerTreeNode {

        @Synthetic
        public FragmentRequestManagerTreeNode() {
        }

        @Override
        public @NotNull Set<RequestManager> getDescendants() {
            Set<RequestManagerFragment> descendantFragments = getDescendantRequestManagerFragments();
            Set<RequestManager> descendants = new HashSet<>(descendantFragments.size());
            for(RequestManagerFragment fragment : descendantFragments){
                if(fragment.getRequestManager() != null){
                    descendants.add(fragment.getRequestManager());
                }
            }
            return descendants;
        }

        @Override
        public String toString() {
            return super.toString() + "{fragment=" + RequestManagerFragment.this + "}";
        }
    }
}
