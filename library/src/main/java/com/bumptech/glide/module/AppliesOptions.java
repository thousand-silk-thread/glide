package com.bumptech.glide.module;

import com.bumptech.glide.GlideBuilder;
import ohos.app.Context;
import org.jetbrains.annotations.NotNull;

/** An internal interface, to be removed when {@link GlideModule}s are removed. */
@Deprecated
interface AppliesOptions {
  /**
   * Lazily apply options to a {@link com.bumptech.glide.GlideBuilder} immediately before the Glide
   * singleton is created.
   *
   * <p>This method will be called once and only once per implementation.
   *
   * @param context An Application {@link ohos.app.Context}.
   * @param builder The {@link com.bumptech.glide.GlideBuilder} that will be used to create Glide.
   */
  void applyOptions(@NotNull Context context, @NotNull GlideBuilder builder);
}
