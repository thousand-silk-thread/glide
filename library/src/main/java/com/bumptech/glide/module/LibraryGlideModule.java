package com.bumptech.glide.module;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import ohos.app.Context;
import org.jetbrains.annotations.NotNull;

/**
 * Registers a set of components to use when initializing Glide within an app when Glide's
 * annotation processor is used.
 *
 * <p>Any number of LibraryGlideModules can be contained within any library or application.
 *
 * <p>LibraryGlideModules are called in no defined order. If LibraryGlideModules within an
 * application conflict, {@link AppGlideModule}s can use the {@link
 * com.bumptech.glide.annotation.Excludes} annotation to selectively remove one or more of the
 * conflicting modules.
 */
@SuppressWarnings("deprecation")
public abstract class LibraryGlideModule implements RegistersComponents {
  @Override
  public void registerComponents(
          @NotNull Context context, @NotNull Glide glide, @NotNull Registry registry) {
    // Default empty impl.
  }
}
