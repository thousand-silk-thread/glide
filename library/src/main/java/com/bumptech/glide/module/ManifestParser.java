package com.bumptech.glide.module;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import ohos.app.Context;

/**
 * Parses {@link com.bumptech.glide.module.GlideModule} references out of the openharmony file.
 */
// Used only in javadoc.
@SuppressWarnings("deprecation")
@Deprecated
public final class ManifestParser {
  private static final String TAG = "ManifestParser";
  private static final String GLIDE_MODULE_VALUE = "GlideModule";

  private final Context context;

  public ManifestParser(Context context) {
    this.context = context;
  }

  @SuppressWarnings("deprecation")
  public List<GlideModule> parse() {
  /*
    if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
      //LogUtil.info(TAG, "Loading Glide modules");
    }
    List<GlideModule> modules = new ArrayList<>();
    // try { //TODO:
    ApplicationInfo appInfo = new ApplicationInfo();
    appInfo.metaData = null;
    // context
    // .getBundleManager()
    // .getApplicationInfo(context.getPackageName(), 0);
    if (appInfo.metaData == null) {
      if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
        //LogUtil.info(TAG, "Got null app info metadata");
      }
      return modules;
    }
    if (LogUtil.isLoggable(TAG, LogUtil.VERBOSE)) {
      //LogUtil.info(TAG, "Got app info metadata: " + appInfo.metaData);
    }
    for (String key : appInfo.metaData.keySet()) {
      if (GLIDE_MODULE_VALUE.equals(appInfo.metaData.get(key))) {
        modules.add(parseModule(key));
        if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
          //LogUtil.info(TAG, "Loaded Glide module: " + key);
        }
      }
    }
    // } catch (PackageManager.NameNotFoundException e) {
    // throw new RuntimeException("Unable to find metadata to parse GlideModules", e);
    // }
    if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
      //LogUtil.info(TAG, "Finished loading Glide modules");
    }

    return modules;
    */
    return null;
    
  }

  @SuppressWarnings("deprecation")
  private static GlideModule parseModule(String className) {
    Class<?> clazz;
    try {
      clazz = Class.forName(className);
    } catch (ClassNotFoundException e) {
      throw new IllegalArgumentException("Unable to find GlideModule implementation", e);
    }

    Object module = null;
    try {
      module = clazz.getDeclaredConstructor().newInstance();
      // These can't be combined until API minimum is 19.
    } catch (InstantiationException e) {
      throwInstantiateGlideModuleException(clazz, e);
    } catch (IllegalAccessException e) {
      throwInstantiateGlideModuleException(clazz, e);
    } catch (NoSuchMethodException e) {
      throwInstantiateGlideModuleException(clazz, e);
    } catch (InvocationTargetException e) {
      throwInstantiateGlideModuleException(clazz, e);
    }

    if (!(module instanceof GlideModule)) {
      throw new RuntimeException("Expected instanceof GlideModule, but found: " + module);
    }
    return (GlideModule) module;
  }

  private static void throwInstantiateGlideModuleException(Class<?> clazz, Exception e) {
    throw new RuntimeException("Unable to instantiate GlideModule implementation for " + clazz, e);
  }
}
