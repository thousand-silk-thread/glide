package com.bumptech.glide;

import ohos.agp.components.element.Element;
import ohos.media.image.PixelMap;

import java.io.File;
import java.net.URL;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Ensures that the set of explicitly supported model types remains consistent across Glide's API
 * surface.
 */
interface ModelTypes<T> {

  T load(Uri uri);

  @NotNull
  T load(@Nullable PixelMap map);

  @NotNull
  T load(@Nullable Element drawable);

  @NotNull
  T load(@Nullable String string);

  @NotNull
  T load(@Nullable File file);

  @NotNull
  T load(@Nullable Integer resourceId);

  @Deprecated
  T load(URL url);

  T load(@Nullable byte[] model);

  @NotNull
  @SuppressWarnings("unchecked")
  T load(@Nullable Object model);
}
