package com.bumptech.glide.load.model;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.util.LogUtil;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.io.File;

/** Loads the file path for {@link MediaStore} owned {@link Uri uris}. */
public final class MediaStoreFileLoader implements ModelLoader<Uri, File> {

  private final Context context;

  // Public API.
  @SuppressWarnings("WeakerAccess")
  public MediaStoreFileLoader(Context context) {
    this.context = context;
  }

  @Override
  public LoadData<File> buildLoadData(
          @NotNull Uri uri, int width, int height,@NotNull Options options) {
    return null; // TODO:
  }

  @Override
  public boolean handles(@NotNull Uri uri) {
    return false;
  }

  private static class FilePathFetcher implements DataFetcher<File> {
    private static final String[] PROJECTION =
        new String[] {
          null,
        };

    private final Context context;
    private final Uri uri;

    FilePathFetcher(Context context, Uri uri) {
      this.context = context;
      this.uri = uri;
    }

    @Override
    public void loadData(@NotNull Priority priority, @NotNull DataCallback<? super File> callback) {
      return; // TODO:
    }

    @Override
    public void cleanup() {
      // Do nothing.
    }

    @Override
    public void cancel() {
      // Do nothing.
    }

    @NotNull
    @Override
    public Class<File> getDataClass() {
      return File.class;
    }

    @NotNull
    @Override
    public DataSource getDataSource() {
      return DataSource.LOCAL;
    }
  }

  /** {@link ModelLoaderFactory} for {@link MediaStoreFileLoader}s. */
  public static final class Factory implements ModelLoaderFactory<Uri, File> {

    private final Context context;

    public Factory(Context context) {
      this.context = context;
    }

    @NotNull
    @Override
    public ModelLoader<Uri, File> build(MultiModelLoaderFactory multiFactory) {
      return new MediaStoreFileLoader(context);
    }

    @Override
    public void teardown() {
      // Do nothing.
    }
  }
}
