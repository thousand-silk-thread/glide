package com.bumptech.glide.load.resource.bitmap;

import ohos.media.image.PixelMap;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.data.BufferedOutputStream;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.util.pool.GlideTrace;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import ohos.media.image.ImagePacker;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class BitmapEncoder implements ResourceEncoder<PixelMap> {
  /**
   * An integer option between 0 and 100 that is used as the compression quality.
   *
   * <p>Defaults to 90.
   */
  public static final Option<Integer> COMPRESSION_QUALITY =
      Option.memory("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);


  private static final String TAG = "BitmapEncoder";
  @Nullable
  private final ArrayPool arrayPool;

  public BitmapEncoder(@NotNull ArrayPool arrayPool) {
    this.arrayPool = arrayPool;
  }

  /** @deprecated Use {@link #BitmapEncoder(ArrayPool)} instead. */
  @Deprecated
  public BitmapEncoder() {
    arrayPool = null;
  }

  @Override
  public boolean encode(
      @NotNull  Resource<PixelMap> resource, @NotNull   File file, @NotNull  Options options) {

    final PixelMap pixelMap = resource.get();
    String format = getFormat(pixelMap, options);

    long start = LogTime.getLogTime();
    int quality = options.get(COMPRESSION_QUALITY);

    boolean success = false;
    OutputStream os = null;
    try {
        os = new FileOutputStream(file);
        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = format;
        packingOptions.quality = 100;			//TODO Later
        packingOptions.numberHint = 1;
        boolean result = imagePacker.initializePacking(os, packingOptions);
        imagePacker.addImage(pixelMap);
        imagePacker.finalizePacking();
        imagePacker.release();
        os.close();

        success = true;
      } catch (IOException e) {

        //LogUtil.info(TAG, "Failed to encode Bitmap");
      } finally {
        if (os != null) {
          try {
            os.close();
          } catch (IOException e) {
            // Do nothing.
          }
        }
      }
      return success;
  }

  private String getFormat(PixelMap pixelMap, Options options) {
    //Bitmap.CompressFormat format = options.get(COMPRESSION_FORMAT);
    //if (format != null) {
     // return format;
    //} else 
    if (ohos.media.image.common.AlphaType.PREMUL == pixelMap.getImageInfo().alphaType) {
      return "image/png";
    } else {
      return "image/jpeg";
    }
  }

  
  @NotNull
  @Override
  public EncodeStrategy getEncodeStrategy(@NotNull Options options) {
    return EncodeStrategy.TRANSFORMED;
  }
}
