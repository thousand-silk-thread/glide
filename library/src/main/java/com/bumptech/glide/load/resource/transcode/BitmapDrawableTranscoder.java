package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.LazyBitmapDrawableResource;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Preconditions;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;

/**
 * An {@link com.bumptech.glide.load.resource.transcode.ResourceTranscoder} that converts
 * Bitmaps into BitmapDrawables.
 */
public class BitmapDrawableTranscoder
    implements ResourceTranscoder<PixelMap, PixelMapElement> {
  private final Context context;

  @Deprecated
  public BitmapDrawableTranscoder(
      @NotNull Context context,
      @SuppressWarnings("unused") BitmapPool bitmapPool) {
    this(context);
  }

  public BitmapDrawableTranscoder(@NotNull Context context) {
    this.context = Preconditions.checkNotNull(context);
  }

  
  @Override
  public Resource<PixelMapElement> transcode(
       @NotNull Resource<PixelMap> toTranscode, @NotNull Options options) {
    return LazyBitmapDrawableResource.obtain(context, toTranscode);
  }
}
