/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bumptech.glide.load.resource.gif.drawableability;

import ohos.agp.components.element.ShapeElement;

import ohos.agp.render.ColorFilter;
import ohos.agp.render.ColorMatrix;

import ohos.agp.utils.Rect;

import java.lang.ref.WeakReference;

/**
 * add some ability that the system don't have
 */
public abstract class RootShapeElement extends ShapeElement implements LevelElement, DraweeViewLifeCycle {
    private final String TAG = RootShapeElement.class.getSimpleName();
    private int mLevel = 0;
    private int mIntrinsicWidth;
    private int mIntrinsicHeight;

    private WeakReference<Callback> mHmCallback = null;

    /**
     * 返回Element固有宽度
     *
     * @return IntrinsicWidth
     */
    public int getIntrinsicWidth() {
        return -1;
    }

    /**
     * 设置 Callback
     *
     * @param callback Callback
     */
    public void setHmCallback(Callback callback) {
        mHmCallback = callback != null ? new WeakReference<>(callback) : null;
    }

    /**
     * 获取 Callback
     *
     * @return Callback
     */
    public Callback getHmCallback() {
        return mHmCallback != null ? mHmCallback.get() : null;
    }

    @Override
    public boolean setVisible(boolean visible, boolean restart) {
        boolean changed = super.setVisible(visible, restart);
        if (changed) {
            invalidateSelf();
        }
        return changed;
    }

    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        Rect oldBounds = getBounds();
        if (oldBounds.left != left || oldBounds.top != top || oldBounds.right != right || oldBounds.bottom != bottom) {
            if (!oldBounds.isEmpty()) {
                // first invalidate the previous bounds
                invalidateSelf();
            }
            super.setBounds(left, top, right, bottom);
            onBoundsChange(getBounds());
        }
    }

    @Override
    public void setColorMatrix(ColorMatrix matrix) {
        super.setColorMatrix(matrix);
    }

    /**
     * setIntrinsicWidth
     *
     * @param intrinsicWidth IntrinsicWidth
     */
    public void setIntrinsicWidth(int intrinsicWidth) {
        this.mIntrinsicWidth = intrinsicWidth;
    }

    /**
     * getIntrinsicHeight
     *
     * @return int
     */
    public int getIntrinsicHeight() {
        return -1;
    }

    /**
     * setIntrinsicHeight
     *
     * @param intrinsicHeight int
     */
    public void setIntrinsicHeight(int intrinsicHeight) {
        this.mIntrinsicHeight = intrinsicHeight;
    }

    /**
     * 获取Level
     *
     * @return int
     */
    public int getLevel() {
        return mLevel;
    }

    /**
     * 设置Level
     *
     * @param level int
     * @return boolean
     */
    public boolean setLevel(int level) {
        if (mLevel != level) {
            mLevel = level;
            return onLevelChange(level);
        }
        return false;
    }

    /**
     * 设置onLevelChange
     *
     * @param level int
     * @return boolean
     */
    public boolean onLevelChange(int level) {
        return false;
    }

    /**
     * 重绘
     */
    public void invalidateSelf() {
        final Callback callback = getHmCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        } else {
        }
    }

    /**
     * 定时 重绘
     *
     * @param what Runnable
     * @param when long 时间
     */
    public void scheduleSelf(Runnable what, long when) {
        final Callback callback = getHmCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, what, when);
        } else {
        }
    }

    /**
     * 撤销重绘绑定
     *
     * @param what Runnable
     */
    public void unscheduleSelf(Runnable what) {
        final Callback callback = getHmCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, what);
        } else {
        }
    }

    /**
     * onBoundsChange
     * @param bounds Rect
     */
    protected void onBoundsChange(Rect bounds) {
        // Stub method.
    }

    /**
     * 颜色过滤器
     * @param colorFilter ColorFilter
     */
    public abstract void setColorFilter(ColorFilter colorFilter);
}
