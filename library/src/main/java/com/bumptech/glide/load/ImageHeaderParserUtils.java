package com.bumptech.glide.load;

import com.bumptech.glide.load.ImageHeaderParser.ImageType;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

import static com.bumptech.glide.load.ImageHeaderParser.ImageType.JPEG;

/** Utilities for the ImageHeaderParser. */
public final class ImageHeaderParserUtils {
  // 5MB. This is the max image header size we can handle, we preallocate a much smaller buffer but
  // will resize up to this amount if necessary.
  private static final int MARK_READ_LIMIT = 5 * 1024 * 1024;

  private ImageHeaderParserUtils() {}

  /** Returns the ImageType for the given InputStream.
   * @param is
   * @param byteArrayPool
   * @param parsers
   * @return imagetype
   * @throws IOException
   */
  @NotNull
  public static ImageType getType(
      @NotNull List<ImageHeaderParser> parsers,
      @Nullable InputStream is,
      @NotNull ArrayPool byteArrayPool)
      throws IOException {
    if (is == null) {
      return ImageType.UNKNOWN;
    }

    if (!is.markSupported()) {
      is = new RecyclableBufferedInputStream(is, byteArrayPool);
    }

    is.mark(MARK_READ_LIMIT);
    final InputStream finalIs = is;
    return getTypeInternal(
        parsers,
        new TypeReader() {
          @Override
          public ImageType getType(ImageHeaderParser parser) throws IOException {
            try {
              return parser.getType(finalIs);
            } finally {
              finalIs.reset();
            }
          }
        });
  }

  /** Returns the ImageType for the given ByteBuffer.
   * @param parsers
   * @param buffer
   * @return imagetype
   * @throws IOException
   */
  @NotNull
  public static ImageType getType(
      @NotNull List<ImageHeaderParser> parsers, @Nullable final ByteBuffer buffer)
      throws IOException {
    if (buffer == null) {
      return ImageType.UNKNOWN;
    }

    return getTypeInternal(
        parsers,
        new TypeReader() {
          @Override
          public ImageType getType(ImageHeaderParser parser) throws IOException {
            return parser.getType(buffer);
          }
        });
  }

  @NotNull
  public static ImageType getType(
      @NotNull List<ImageHeaderParser> parsers,
      @NotNull final ArrayPool byteArrayPool)
      throws IOException {
    return getTypeInternal(
        parsers,
        new TypeReader() {
          @Override
          public ImageType getType(ImageHeaderParser parser) throws IOException {
            // Wrap the FileInputStream into a RecyclableBufferedInputStream to optimize I/O
            // performance
            InputStream is = null;
            try {

              return JPEG;
            } finally {
              try {
                if (is != null) {
                  is.close();
                }
              } catch (IOException e) {
                // Ignored.
              }

            }
          }
        });
  }

  
  @NotNull
  private static ImageType getTypeInternal(
      @NotNull List<ImageHeaderParser> parsers, TypeReader reader) throws IOException {
    //noinspection ForLoopReplaceableByForEach to improve perf
    for (int i = 0, size = parsers.size(); i < size; i++) {
      ImageHeaderParser parser = parsers.get(i);
      ImageType type = reader.getType(parser);
      if (type != ImageType.UNKNOWN) {
        return type;
      }
    }

    return ImageType.UNKNOWN;
  }

  /** Returns the orientation for the given InputStream.
   * @param parsers
   * @param byteArrayPool
   * @param is
   * @return int
   * @throws IOException
   */
  public static int getOrientation(
      @NotNull List<ImageHeaderParser> parsers,
      @Nullable InputStream is,
      @NotNull final ArrayPool byteArrayPool)
      throws IOException {
    if (is == null) {
      return ImageHeaderParser.UNKNOWN_ORIENTATION;
    }

    if (!is.markSupported()) {
      is = new RecyclableBufferedInputStream(is, byteArrayPool);
    }

    is.mark(MARK_READ_LIMIT);
    final InputStream finalIs = is;
    return getOrientationInternal(
        parsers,
        new OrientationReader() {
          @Override
          public int getOrientation(ImageHeaderParser parser) throws IOException {
            try {
              return parser.getOrientation(finalIs, byteArrayPool);
            } finally {
              finalIs.reset();
            }
          }
        });
  }

  public static int getOrientation(
      @NotNull List<ImageHeaderParser> parsers,
      @NotNull final ArrayPool byteArrayPool)
      throws IOException {
    return getOrientationInternal(
        parsers,
        new OrientationReader() {
          @Override
          public int getOrientation(ImageHeaderParser parser) throws IOException {
            // Wrap the FileInputStream into a RecyclableBufferedInputStream to optimize I/O
            // performance
            InputStream is = null;
            try {
            
              return 0;
            } finally {
              try {
                if (is != null) {
                  is.close();
                }
              } catch (IOException e) {
                // Ignored.
              }
            }
          }
        });
  }

  private static int getOrientationInternal(
      @NotNull List<ImageHeaderParser> parsers, OrientationReader reader) throws IOException {
    //noinspection ForLoopReplaceableByForEach to improve perf
    for (int i = 0, size = parsers.size(); i < size; i++) {
      ImageHeaderParser parser = parsers.get(i);
      int orientation = reader.getOrientation(parser);
      if (orientation != ImageHeaderParser.UNKNOWN_ORIENTATION) {
        return orientation;
      }
    }

    return ImageHeaderParser.UNKNOWN_ORIENTATION;
  }

  private interface TypeReader {
    ImageType getType(ImageHeaderParser parser) throws IOException;
  }

  private interface OrientationReader {
    int getOrientation(ImageHeaderParser parser) throws IOException;
  }
}
