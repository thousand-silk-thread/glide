package com.bumptech.glide.load.resource.transcode;



import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Transcodes a resource of one type to a resource of another type.
 *
 * @param <Z> The type of the resource that will be transcoded from.
 * @param <R> The type of the resource that will be transcoded to.
 */
public interface ResourceTranscoder<Z, R> {

  /**
   * Transcodes the given resource to the new resource type and returns the new resource.
   *
   * @param toTranscode The resource to transcode.
   * @param options
   * @return resource
   */
  @Nullable
  Resource<R> transcode(@NotNull Resource<Z> toTranscode, @NotNull Options options);
}
