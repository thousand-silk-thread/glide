package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;


public class DrawableTransformation implements Transformation<Element> {

  private static final String TAG = "DrawableTransformation";
  private final Transformation<PixelMap> wrapped;
  private final boolean isRequired;

  public DrawableTransformation(Transformation<PixelMap> wrapped, boolean isRequired) {
    this.wrapped = wrapped;
    this.isRequired = isRequired;
  }

  @SuppressWarnings("unchecked")
  public Transformation<PixelMapElement> asBitmapDrawable() {
    return (Transformation<PixelMapElement>) (Transformation<?>) this;
  }

  @NotNull
  @Override
  public Resource<Element> transform(Context context, @NotNull Resource<Element> resource, int outWidth, int outHeight) {
    BitmapPool bitmapPool = Glide.get(context).getBitmapPool();
    Element drawable = resource.get();
    Resource<PixelMap> bitmapResourceToTransform =
        DrawableToBitmapConverter.convert(bitmapPool, drawable, outWidth, outHeight);
    if (bitmapResourceToTransform == null) {
      if (isRequired) {
        throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
      } else {
        return resource;
      }
    }
    Resource<PixelMap> transformedBitmapResource =
        wrapped.transform(context, bitmapResourceToTransform, outWidth, outHeight);

    if (transformedBitmapResource.equals(bitmapResourceToTransform)) {
      transformedBitmapResource.recycle();
      return resource;
    } else {
      return newDrawableResource(context, transformedBitmapResource);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof DrawableTransformation) {
      DrawableTransformation other = (DrawableTransformation) o;
      return wrapped.equals(other.wrapped);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return wrapped.hashCode();
  }

  @Override
  public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
    wrapped.updateDiskCacheKey(messageDigest);
  }


    @SuppressWarnings({"unchecked", "PMD.UnnecessaryLocalBeforeReturn"})
    private Resource<Element> newDrawableResource(Context context, Resource<PixelMap> transformed) {
    Resource<? extends Element> result =
        LazyBitmapDrawableResource.obtain(context, transformed);
    return (Resource<Element>) result;
  }
}
