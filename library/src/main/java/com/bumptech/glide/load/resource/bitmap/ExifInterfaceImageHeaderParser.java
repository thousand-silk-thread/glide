package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Uses {@link ExifInterface} to parse orientation data.
 *
 * <p>ExifInterface supports the HEIF format on OMR1+. Glide's {@link DefaultImageHeaderParser}
 * doesn't currently support HEIF. In the future we should reconcile these two classes, but for now
 * this is a simple way to ensure that HEIF files are oriented correctly on platforms where they're
 * supported.
 */
public final class ExifInterfaceImageHeaderParser implements ImageHeaderParser {

  
  @NotNull
  @Override
  public ImageType getType(@NotNull InputStream is) {
    return ImageType.UNKNOWN;
  }

  
  @NotNull
  @Override
  public ImageType getType(@NotNull ByteBuffer byteBuffer) {
    return ImageType.UNKNOWN;
  }

  @Override
  public int getOrientation(@NotNull InputStream is, @NotNull ArrayPool byteArrayPool)
      throws IOException {
    return ImageHeaderParser.UNKNOWN_ORIENTATION;
  }

  @Override
  public int getOrientation(@NotNull ByteBuffer byteBuffer, @NotNull ArrayPool byteArrayPool)
      throws IOException {
    return getOrientation(ByteBufferUtil.toStream(byteBuffer), byteArrayPool);
  }
}
