/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bumptech.glide.load.resource.gif.drawableability;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * MainHandler
 */
public class MainHandler {
    private static EventHandler eventHandler;

    /**
     * 是否是主线程
     * @return boolean
     */
    public static boolean isInMainThread() {
        return Thread.currentThread().getId() == EventRunner.getMainEventRunner().getThreadId();
    }

    /**
     * post 一个线程 主线程执行
     * @param runnable Runnable
     */
    public static void post(Runnable runnable) {
        if (runnable == null) {
            return;
        }
        if (isInMainThread()) {
            runnable.run();
        } else {
            if (eventHandler == null) {
                eventHandler = new EventHandler(EventRunner.getMainEventRunner());
            }
            eventHandler.postTask(runnable);
        }
    }

    /**
     * eventHandler 移除 Runnable
     * @param runnable Runnable
     */
    public static void removeTask(Runnable runnable){
        if(eventHandler == null){
            return;
        }
        eventHandler.removeTask(runnable);
    }

    /**
     * removeAllTasks
     */
    public static void removeAllTasks(){
        if(eventHandler == null){
            return;
        }
        eventHandler.removeAllEvent();
    }

    /**
     * postDelay
     * @param runnable Runnable
     * @param timeInMillSeconds timeInMillSeconds
     */
    public static void postDelay(Runnable runnable, long timeInMillSeconds) {
        if (runnable == null) {
            return;
        }
        if (eventHandler == null) {
            eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        }
        eventHandler.postTask(runnable, timeInMillSeconds);
    }
}
