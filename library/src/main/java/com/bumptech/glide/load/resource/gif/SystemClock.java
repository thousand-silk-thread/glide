package com.bumptech.glide.load.resource.gif;

import ohos.miscservices.timeutility.Time;

/**
 * SystemClock
 */
public class SystemClock {
    /**
     * uptimeMillis
     * @return long
     */
    public static long uptimeMillis(){
        return Time.getRealActiveTime();
    }

    /**
     * elapsedRealtime
     * @return long
     */
    public static long elapsedRealtime(){
        return Time.getRealTime();
    }
}