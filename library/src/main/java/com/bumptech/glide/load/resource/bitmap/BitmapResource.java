package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/** A resource wrapping a Bitmap object. */
public class BitmapResource implements Resource<PixelMap>, Initializable {
  private final PixelMap pixelmap;
  private final BitmapPool pixelmapPool;


  
  @Nullable
  public static BitmapResource obtain(@Nullable PixelMap pixelmap, @Nullable BitmapPool pixelmapPool) {
    if (pixelmap == null) {
      return null;
    } else {
      return new BitmapResource(pixelmap, pixelmapPool);
    }
  }

  public BitmapResource( PixelMap pixelmap,  BitmapPool pixelmapPool) {
    this.pixelmap = Preconditions.checkNotNull(pixelmap, "PixelMap must not be null");
    this.pixelmapPool = Preconditions.checkNotNull(pixelmapPool, "pixelmapPool must not be null");
  }

  
  @NotNull
  @Override
  public Class<PixelMap> getResourceClass() {
    return PixelMap.class;
  }

  
  @NotNull
  @Override
  public PixelMap get() {
    return pixelmap;
  }

  @Override
  public int getSize() {
    return Util.getBitmapByteSize(pixelmap);
  }

  @Override
  public void recycle() {
    pixelmapPool.put(pixelmap);
  }

  @Override
  public void initialize() {
    //pixelmap.prepareToDraw(); //TODO: , pixelmap API is not yet ready
  }
}
