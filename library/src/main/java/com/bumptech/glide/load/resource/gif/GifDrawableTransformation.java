package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.util.Preconditions;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.security.MessageDigest;

/**
 * An {@link Transformation} that wraps a transformation for a {@link
 * Bitmap} and can apply it to every frame of any {@link
 * GifDrawable}.
 */
public class GifDrawableTransformation implements Transformation<GifDrawable> {
  private final Transformation<PixelMap> wrapped;

  public GifDrawableTransformation(Transformation<PixelMap> wrapped) {
    this.wrapped = Preconditions.checkNotNull(wrapped);
  }

  @Override
  public Resource<GifDrawable> transform(
       Context context,
       Resource<GifDrawable> resource,
      int outWidth,
      int outHeight) {
    GifDrawable drawable = resource.get();

    // The drawable needs to be initialized with the correct width and height in order for a view
    // displaying it to end up with the right dimensions. Since our transformations may arbitrarily
    // modify the dimensions of our GIF, here we create a stand in for a frame and pass it to the
    // transformation to see what the final transformed dimensions will be so that our drawable can
    // report the correct intrinsic width and height.
    BitmapPool bitmapPool = Glide.get(context).getBitmapPool();
    PixelMap firstFrame = drawable.getFirstFrame();
    Resource<PixelMap> bitmapResource = new BitmapResource(firstFrame, bitmapPool);
    Resource<PixelMap> transformed = wrapped.transform(context, bitmapResource, outWidth, outHeight);
    if (!bitmapResource.equals(transformed)) {
      bitmapResource.recycle();
    }
    PixelMap transformedFrame = transformed.get();

    drawable.setFrameTransformation(wrapped, transformedFrame);
    return resource;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof GifDrawableTransformation) {
      GifDrawableTransformation other = (GifDrawableTransformation) o;
      return wrapped.equals(other.wrapped);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return wrapped.hashCode();
  }

  @Override
  public void updateDiskCacheKey(MessageDigest messageDigest) {
    wrapped.updateDiskCacheKey(messageDigest);
  }
}
