package com.bumptech.glide.load.resource.gif;

import ohos.agp.components.Component;

/**
 * Animatable
 */
public interface Animatable {
    /**
     * Starts the drawable's animation.
     */
    void start();
    void registView(Component component);

    /**
     * Stops the drawable's animation.
     */
    void stop();

    /**
     * Indicates whether the animation is running.
     *
     * @return True if the animation is running, false otherwise.
     */
    boolean isRunning();
}
