package com.bumptech.glide.load.resource.gif;


import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.jetbrains.annotations.Nullable;

/**
 * Implements {@link GifDecoder.BitmapProvider} by wrapping Glide's
 * {@link BitmapPool}.
 */
public final class GifBitmapProvider implements GifDecoder.BitmapProvider {
  private final BitmapPool bitmapPool;
  @Nullable
  private final ArrayPool arrayPool;

  /**
   * Constructs an instance without a shared byte array pool. Byte arrays will be always constructed
   * when requested.
   */
  public GifBitmapProvider(BitmapPool bitmapPool) {
    this(bitmapPool, /*arrayPool=*/ null);
  }

  /**
   * Constructs an instance with a shared array pool. Arrays will be reused where possible.
   * @param bitmapPool BitmapPool
   * @param arrayPool ArrayPool
   */
  @SuppressWarnings("WeakerAccess")
  public GifBitmapProvider(BitmapPool bitmapPool, @Nullable ArrayPool arrayPool) {
    this.bitmapPool = bitmapPool;
    this.arrayPool = arrayPool;
  }


  @Override
  public PixelMap obtain(int width, int height, PixelFormat config) {
    return bitmapPool.getDirty(width, height, config);
  }

  @Override
  public void release(PixelMap bitmap) {
    bitmapPool.put(bitmap);
  }


  @Override
  public byte[] obtainByteArray(int size) {
    if (arrayPool == null) {
      return new byte[size];
    }
    return arrayPool.get(size, byte[].class);
  }

  @Override
  public void release(byte[] bytes) {
    if (arrayPool == null) {
      return;
    }
    arrayPool.put(bytes);
  }

  @Override
  public int[] obtainIntArray(int size) {
    if (arrayPool == null) {
      return new int[size];
    }
    return arrayPool.get(size, int[].class);
  }

  @SuppressWarnings("PMD.UseVarargs")
  @Override
  public void release(int[] array) {
    if (arrayPool == null) {
      return;
    }
    arrayPool.put(array);
  }
}
