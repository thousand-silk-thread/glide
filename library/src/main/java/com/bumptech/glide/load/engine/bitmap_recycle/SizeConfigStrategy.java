package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.util.Synthetic;
import com.bumptech.glide.util.Util;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Keys Bitmaps using both Bitmap#getAllocationByteCount() and the Bitmap.Config
 * returned from Bitmap#getConfig().
 *
 * <p>Using both the config and the byte size allows us to safely re-use a greater variety of
 * Bitmaps, which increases the hit rate of the pool and therefore the
 * performance of applications. This class works around #301 by only allowing re-use of
 * Bitmaps with a matching number of bytes per pixel.
 */
public class SizeConfigStrategy implements LruPoolStrategy {
  private static final int MAX_SIZE_MULTIPLE = 8;

  private static final ohos.media.image.common.PixelFormat[] ARGB_8888_IN_CONFIGS;
  private static final String TAG = "SizeConfigStrategy";

  static {
    ohos.media.image.common.PixelFormat[] result =
        new ohos.media.image.common.PixelFormat[] {
          ohos.media.image.common.PixelFormat.ARGB_8888,
          // The value returned by Bitmaps with the hidden Bitmap config.
          null,
        };   
    ARGB_8888_IN_CONFIGS = result;
  }

  // We probably could allow ARGB_4444 and RGB_565 to decode into each other, but ARGB_4444 is
  // deprecated and we'd rather be safe.
  private static final ohos.media.image.common.PixelFormat[] RGB_565_IN_CONFIGS =
      new ohos.media.image.common.PixelFormat[] {ohos.media.image.common.PixelFormat.RGB_565};

  private final KeyPool keyPool = new KeyPool();
  private final GroupedLinkedMap<Key, PixelMap> groupedMap = new GroupedLinkedMap<>();
  private final Map<ohos.media.image.common.PixelFormat, NavigableMap<Integer, Integer>> sortedSizes = new HashMap<>();

  @Override
  public void put(PixelMap pixelmap) {

    int size = Util.getBitmapByteSize(pixelmap);
    Key key = keyPool.get(size, pixelmap.getImageInfo().pixelFormat);

    groupedMap.put(key, pixelmap);

    NavigableMap<Integer, Integer> sizes = getSizesForConfig(pixelmap.getImageInfo().pixelFormat);
    Integer current = sizes.get(key.size);
    sizes.put(key.size, current == null ? 1 : current + 1);
  }

  @Nullable
  @Override  
  public PixelMap get(int width, int height, ohos.media.image.common.PixelFormat config) {

    int imgsize = Util.getBitmapByteSize(width, height, config);
    Key bestKey = findBestKey(imgsize, config);

    PixelMap result = groupedMap.get(bestKey);
    if (result != null) {
      // Decrement must be called before reconfigure.
      decrementBitmapOfSize(bestKey.size, result);
      Size size= new Size();
      size.width = width;
      size.height = height;
      
      result.resetConfig( size, config);
    }
    return result;
  }

  private Key findBestKey(int size, ohos.media.image.common.PixelFormat config) {

    Key result = keyPool.get(size, config);
    for (ohos.media.image.common.PixelFormat possibleConfig : getInConfigs(config)) {              //Check this
      NavigableMap<Integer, Integer> sizesForPossibleConfig = getSizesForConfig(possibleConfig);
      Integer possibleSize = sizesForPossibleConfig.ceilingKey(size);
      if (possibleSize != null && possibleSize <= size * MAX_SIZE_MULTIPLE) {
        if (possibleSize != size
            || (possibleConfig == null ? config != null : !possibleConfig.equals(config))) {
          keyPool.offer(result);
          result = keyPool.get(possibleSize, possibleConfig);
        }
        break;
      }
    }
    return result;
  }

  @Override
  @Nullable
  public PixelMap removeLast() {

    PixelMap removed = groupedMap.removeLast();
    if (removed != null) {
      int removedSize = Util.getBitmapByteSize(removed);
      decrementBitmapOfSize(removedSize, removed);
    }
    return removed;
  }

  private void decrementBitmapOfSize(Integer size, PixelMap removed) {

    ohos.media.image.common.PixelFormat config = removed.getImageInfo().pixelFormat;
    NavigableMap<Integer, Integer> sizes = getSizesForConfig(config);
    Integer current = sizes.get(size);
    if (current == null) {
      throw new NullPointerException(
          "Tried to decrement empty size"
              + ", size: "
              + size
              + ", removed: "
              + logBitmap(removed)
              + ", this: "
              + this);
    }

    if (current == 1) {
      sizes.remove(size);
    } else {
      sizes.put(size, current - 1);
    }
  }

  private NavigableMap<Integer, Integer> getSizesForConfig(ohos.media.image.common.PixelFormat config) {
    NavigableMap<Integer, Integer> sizes = sortedSizes.get(config);
    if (sizes == null) {
      sizes = new TreeMap<>();
      sortedSizes.put(config, sizes);
    }
    return sizes;
  }

  @Override
  public String logBitmap(PixelMap pixelmap) {
    int size = Util.getBitmapByteSize(pixelmap);
    return getPixelmapString(size, pixelmap.getImageInfo().pixelFormat);
  }

  @Override
  public String logBitmap(int width, int height, ohos.media.image.common.PixelFormat config) {
    int size = Util.getBitmapByteSize(width, height, config);
    return getPixelmapString(size, config);
  }

  @Override
  public int getSize(PixelMap pixelmap) {
    return Util.getBitmapByteSize(pixelmap);
  }

  @Override
  public String toString() {
    StringBuilder sb =
        new StringBuilder()
            .append("SizeConfigStrategy{groupedMap=")
            .append(groupedMap)
            .append(", sortedSizes=(");
    for (Map.Entry<ohos.media.image.common.PixelFormat, NavigableMap<Integer, Integer>> entry : sortedSizes.entrySet()) {
      sb.append(entry.getKey()).append('[').append(entry.getValue()).append("], ");
    }
    if (!sortedSizes.isEmpty()) {
      sb.replace(sb.length() - 2, sb.length(), "");
    }
    return sb.append(")}").toString();
  }

  @VisibleForTesting
  static class KeyPool extends BaseKeyPool<Key> {

    public Key get(int size, ohos.media.image.common.PixelFormat config) {

      Key result = get();
      result.init(size, config);
      return result;
    }

    @Override
    protected Key create() {
      return new Key(this);
    }
  }

  @VisibleForTesting
  static final class Key implements Poolable {
    private final KeyPool pool;

    @Synthetic int size;
    private ohos.media.image.common.PixelFormat config;

    public Key(KeyPool pool) {
      this.pool = pool;
    }

    @VisibleForTesting
    Key(KeyPool pool, int size, ohos.media.image.common.PixelFormat config) {
      this(pool);
      init(size, config);
    }

    public void init(int size, ohos.media.image.common.PixelFormat config) {
      this.size = size;
      this.config = config;
    }

    @Override
    public void offer() {
      pool.offer(this);
    }

    @Override
    public String toString() {
      return getPixelmapString(size, config);
    }

    @Override
    public boolean equals(Object o) {

      if (o instanceof Key) {
        Key other = (Key) o;
        return size == other.size && Util.bothNullOrEqual(config, other.config);
      }
      return false;
    }

    @Override
    public int hashCode() {
      int result = size;
      result = 31 * result + (config != null ? config.hashCode() : 0);
      return result;
    }
  }

  @Synthetic
  static String getPixelmapString(int size, ohos.media.image.common.PixelFormat config) {
    return "[" + size + "](" + config + ")";
  }

  private static ohos.media.image.common.PixelFormat[] getInConfigs(ohos.media.image.common.PixelFormat requested) {    

    switch (requested) {
      case ARGB_8888:
        return ARGB_8888_IN_CONFIGS;
      case RGB_565:
        return RGB_565_IN_CONFIGS;
      default:
        return new ohos.media.image.common.PixelFormat[] {requested};
    }
  }
}
