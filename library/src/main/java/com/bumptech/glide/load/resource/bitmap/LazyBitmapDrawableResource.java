package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Preconditions;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ohos.app.Context;

/**
 * Lazily allocates a {@link PixelMapElement} from a given {@link
 * PixelMap} on the first call to {@link #get()}.
 */
public final class LazyBitmapDrawableResource
    implements Resource<PixelMapElement>, Initializable {

  private final Context context;
  private final Resource<PixelMap> bitmapResource;
  private static final String TAG = "LazyBitmapDrawableR";

  @Deprecated
  public static LazyBitmapDrawableResource obtain(
      Context context, PixelMap pixelMap) {
    return (LazyBitmapDrawableResource)
        obtain(
            context,
            BitmapResource.obtain(pixelMap, Glide.get(context).getBitmapPool()));
  }

  @Deprecated
  public static LazyBitmapDrawableResource obtain(
          Context context, BitmapPool pixelmapPool, PixelMap pixelMap) {
    return (LazyBitmapDrawableResource)
        obtain(context, BitmapResource.obtain(pixelMap, pixelmapPool));
  }

  
  @Nullable
  public static Resource<PixelMapElement> obtain(
          @NotNull Context context, @Nullable  Resource<PixelMap> bitmapResource) {
    if (bitmapResource == null) {
      return null;
    }
    return new LazyBitmapDrawableResource(context, bitmapResource);
  }

  private LazyBitmapDrawableResource(
       @NotNull  Context context,  @NotNull Resource<PixelMap> bitmapResource) {
    this.context = Preconditions.checkNotNull(context);
    this.bitmapResource = Preconditions.checkNotNull(bitmapResource);
  }

  
  @NotNull
  @Override
  public Class<PixelMapElement> getResourceClass() {
    return PixelMapElement.class;
  }

  
  @NotNull
  @Override
  public PixelMapElement get() {
    return new PixelMapElement(bitmapResource.get());
  }

  @Override
  public int getSize() {
    return bitmapResource.getSize();
  }

  @Override
  public void recycle() {
    bitmapResource.recycle();
  }

  @Override
  public void initialize() {
    if (bitmapResource instanceof Initializable) {
      ((Initializable) bitmapResource).initialize();
    }
  }
}
