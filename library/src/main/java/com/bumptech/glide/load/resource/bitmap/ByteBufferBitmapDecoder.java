package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.util.LogUtil;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/** Decodes Bitmaps from {@link ByteBuffer ByteBuffers}. */
public class ByteBufferBitmapDecoder implements ResourceDecoder<ByteBuffer, PixelMap> {
  private final Downsampler downsampler;

  public ByteBufferBitmapDecoder(Downsampler downsampler) {
    this.downsampler = downsampler;
  }

  @Override
  public boolean handles(@NotNull ByteBuffer source, @NotNull Options options) {
    return downsampler.handles(source);
  }

  @Override
  public Resource<PixelMap> decode(
      @NotNull ByteBuffer source, int width, int height, @NotNull Options options)
      throws IOException {

    InputStream is = ByteBufferUtil.toStream(source);
    return downsampler.decode(is, width, height, options);
    
  }
}
