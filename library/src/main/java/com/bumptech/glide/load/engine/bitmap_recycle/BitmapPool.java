package com.bumptech.glide.load.engine.bitmap_recycle;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.jetbrains.annotations.NotNull;

/** An interface for a pool that allows users to reuse {@link PixelMap} objects. */
public interface BitmapPool {

  /** Returns the current maximum size of the pool in bytes.
   * @return long
   */
  long getMaxSize();

  void setSizeMultiplier(float sizeMultiplier);

  void put(PixelMap pixelmap);

  @NotNull
  PixelMap get(int width, int height, PixelFormat config);

  @NotNull
  PixelMap getDirty(int width, int height, PixelFormat config);

  /** Removes all Bitmaps from the pool. */
  void clearMemory();

  /**
   * Reduces the size of the cache by evicting items based on the given level.
   *
   * @param level The level from ComponentCallbacks2 to use to determine how
   *     many Bitmaps to evict.
   */
  void trimMemory(int level);
}
