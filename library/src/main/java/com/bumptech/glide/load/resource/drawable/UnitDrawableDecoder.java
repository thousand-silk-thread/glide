package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import ohos.agp.components.element.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/** Passes through a {@link Element} as a {@link Element} based {@link Resource}. */
public class UnitDrawableDecoder implements ResourceDecoder<Element, Element> {
  @Override
  public boolean handles(@NotNull Element source, @NotNull Options options) {
    return true;
  }

  
  @Nullable
  @Override
  public Resource<Element> decode(
      @NotNull Element source, int width, int height, @NotNull Options options) {
    return NonOwnedDrawableResource.newInstance(source);
  }
}
