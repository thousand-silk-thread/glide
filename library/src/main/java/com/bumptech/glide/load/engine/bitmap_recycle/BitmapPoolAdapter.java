package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.util.LogUtil;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.jetbrains.annotations.NotNull;

/**
 * An {@link com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool BitmapPool} implementation
 * that rejects all {@link PixelMap}s added to it and always returns {@code
 * null} from get.
 */
public class BitmapPoolAdapter implements BitmapPool {
  @Override
  public long getMaxSize() {
    return 0;
  }

  @Override
  public void setSizeMultiplier(float sizeMultiplier) {
    // Do nothing.
  }

  @Override
  public void put(PixelMap pixelmap) {
    pixelmap.release();
  }

  @NotNull
  @Override
  public PixelMap get(int width, int height, PixelFormat config) {
    return PixelMap.create(LruBitmapPool.getdefaultinitOptions(width, height, config)); //TODO:
  }

  @NotNull
  @Override
  public PixelMap getDirty(int width, int height,  PixelFormat config) {
    return get(width, height, config);
  }

  @Override
  public void clearMemory() {
    // Do nothing.
  }

  @Override
  public void trimMemory(int level) {
    // Do nothing.
  }
}
