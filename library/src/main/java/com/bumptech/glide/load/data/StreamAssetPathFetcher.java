package com.bumptech.glide.load.data;

import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.io.IOException;
import java.io.InputStream;

/** Fetches an {@link InputStream} for an asset path. */
public class StreamAssetPathFetcher extends AssetPathFetcher<InputStream> {
  public StreamAssetPathFetcher(Context context, String assetPath) {
    super(context, assetPath);
  }

  @Override
  protected InputStream loadResource(Context context, String path)
      throws IOException {
    return null; // TODO: phase2
  }

  @Override
  protected void close(InputStream data) throws IOException {
    data.close();
  }

  @NotNull
  @Override
  public Class<InputStream> getDataClass() {
    return InputStream.class;
  }
}
