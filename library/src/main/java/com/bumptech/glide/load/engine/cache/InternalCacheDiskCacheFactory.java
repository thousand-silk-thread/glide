package com.bumptech.glide.load.engine.cache;

import ohos.app.Context;

import java.io.File;
import com.bumptech.glide.util.LogUtil;

/**
 * Creates an {@link com.bumptech.glide.disklrucache.DiskLruCache} based disk cache in the internal
 * disk cache directory.
 */
// Public API.
@SuppressWarnings({"WeakerAccess", "unused"})
public final class InternalCacheDiskCacheFactory extends DiskLruCacheFactory {

  public InternalCacheDiskCacheFactory(Context context) {
    this(
        context,
        DiskCache.Factory.DEFAULT_DISK_CACHE_DIR,
        DiskCache.Factory.DEFAULT_DISK_CACHE_SIZE);
  }

  public InternalCacheDiskCacheFactory(Context context, long diskCacheSize) {
    this(context, DiskCache.Factory.DEFAULT_DISK_CACHE_DIR, diskCacheSize);
  }

  public InternalCacheDiskCacheFactory(
      final Context context, final String diskCacheName, long diskCacheSize) {
    super(
        new CacheDirectoryGetter() {
          @Override
          public File getCacheDirectory() {
            File cacheDirectory = context.getCacheDir();
            if (cacheDirectory == null) {
              return null;
            }
            if (diskCacheName != null) {
              File internalcachefile = new File(cacheDirectory, diskCacheName);
			  
		 if (!internalcachefile.exists()) {		 	
                 if (!internalcachefile.mkdirs()) {
		     return null;
                 }		 
              }
		 
		 return internalcachefile;		 
            }
            return cacheDirectory;
          }
        },
        diskCacheSize);
  }
}
