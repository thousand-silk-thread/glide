package com.bumptech.glide.request.target;

import com.bumptech.glide.load.resource.gif.Animatable;
import com.bumptech.glide.request.transition.Transition;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A base {@link Target} for displaying resources in {@link Image}s.
 *
 * @param <Z> The type of resource that this target will display in the wrapped {@link Image}.
 */
// Public API.
@SuppressWarnings("WeakerAccess")
public abstract class ImageViewTarget<Z> extends ViewTarget<Image, Z>
    implements Transition.ViewAdapter {

  private Animatable animatable;

  public ImageViewTarget(Image view) {
    super(view);
  }

  /** @deprecated Use {@link #waitForLayout()} instead. */
  @SuppressWarnings({"deprecation"})
  @Deprecated
  public ImageViewTarget(Image view, boolean waitForLayout) {
    super(view, waitForLayout);
  }


  @Override
  @Nullable
  public Element getCurrentDrawable() {
    return null; // view.getDrawable();//TODO - openharmony doesn't have equivalent interface
  }


  @Override
  public void setDrawable(Element drawable) {
    view.setImageElement( drawable);
  }


  @Override
  public void onLoadStarted(@Nullable Element placeholder) {
    super.onLoadStarted(placeholder);
    setResourceInternal(null);
    setDrawable(placeholder);
  }


  @Override
  public void onLoadFailed(@Nullable Element errorDrawable) {
    super.onLoadFailed(errorDrawable);
    setResourceInternal(null);
    setDrawable(errorDrawable);
  }


  @Override
  public void onLoadCleared(@Nullable Element placeholder) {
    super.onLoadCleared(placeholder);
   if (animatable != null) {
     animatable.stop();
   }
    setResourceInternal(null);
    setDrawable(placeholder);
  }

  @Override
  public void onResourceReady(@NotNull Z resource, @Nullable Transition<? super Z> transition) {
    if (transition == null || !transition.transition(resource, this)) {
      setResourceInternal(resource);
    } else {
      maybeUpdateAnimatable(resource);
    }
  }

  @Override
  public void onStart() {
   if (animatable != null) {
     animatable.start();
   }
  }

  @Override
  public void onStop() {
   if (animatable != null) {
     animatable.stop();
   }
  }

  private void setResourceInternal(@Nullable Z resource) {
    // Order matters here. Set the resource first to make sure that the Drawable has a valid and
    // non-null Callback before starting it.
    setResource(resource);
    maybeUpdateAnimatable(resource);
  }

  private void maybeUpdateAnimatable(@Nullable Z resource) {
   if (resource instanceof Animatable) {
     animatable = (Animatable) resource;
     animatable.start();
   } else {
     animatable = null;
   }
  }

  protected abstract void setResource(@Nullable Z resource);
}
