package com.bumptech.glide.request.target;

import ohos.agp.components.Image;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;

/**
 * A factory responsible for producing the correct type of {@link Target} for a given {@link
 * ohos.agp.components.Component} subclass.
 */
public class ImageViewTargetFactory {
	@NotNull
    @SuppressWarnings("unchecked")
    public <Z> ViewTarget<Image, Z> buildTarget(
            @NotNull Image view, @NotNull Class<Z> clazz) {
        if(PixelMap.class.equals(clazz)){
            return (ViewTarget<Image, Z>) new BitmapImageViewTarget(view);
        }else {
            return (ViewTarget<Image, Z>) new DrawableImageViewTarget(view);
        }
        }
}
