package com.bumptech.glide.request.target;

import com.bumptech.glide.load.resource.gif.drawableability.RootShapeElement;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import org.jetbrains.annotations.Nullable;

/** A target for display {@link Element} objects in {@link Image}s. */
public class DrawableImageViewTarget extends ImageViewTarget<Element> {

  public DrawableImageViewTarget(Image view) {
    super(view);
  }


  /** @deprecated Use {@link #waitForLayout()} instead. */
  // Public API.
  @SuppressWarnings({"unused", "deprecation"})
  @Deprecated
  public DrawableImageViewTarget(Image view, boolean waitForLayout) {
    super(view, waitForLayout);
  }

  @Override
  protected void setResource(@Nullable Element resource) {

    if(resource instanceof PixelMapElement) {
      view.setPixelMap(((PixelMapElement) resource).getPixelMap());
    } else if (resource instanceof RootShapeElement){
      view.setImageElement(resource);
    } else {}
  }
}
