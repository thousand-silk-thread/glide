## Glide

## Introduction:
Glide is a fast and efficient image loading library for openharmony focused on smooth scrolling. Glide offers an easy to use API, a performant and extensible resource decoding pipeline and automatic resource pooling.# Glide

Glide hmos testapplication + ported code project
Glide supports fetching, decoding, and displaying video stills, images, and animated GIFs. Glide includes a flexible api that allows developers to plug in to almost any network stack. By default Glide uses a custom HttpUrlConnection based stack, but also includes utility libraries plug in to Google's Volley project or Square's OkHttp library instead.

Glide primary focus is on making scrolling any kind of a list of images as smooth and fast as possible, but Glide is also effective for almost any case where you need to fetch, resize, and display a remote image.

## Usage Instructions:
API
Glide uses a simple fluent API that allows users to make most requests in a single line:


```
1)Load Image from Internet
 //String can be like "https://picsum.photos/600"
 Glide.with(classcontext)
                    .load(String)  
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);
                    
2)Load Image from Resource Folder
    //ResourceId can be esourceTable.Media_penguins
    Glide.with(classcontext)
            .load(ResourceId) 
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imageView);
            
3) Uri uri =Uri.parse("https://picsum.photos/600");
   Glide.with(classcontext)
        .load(uri)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(imageView);


4)Uri uri =Uri.parse("dataability:///" + "com.bumptech.glide"+ "/" + ResourceTable.Media_jellyfish);
  Glide.with(classcontext)
        .load(uri)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(imageView);
```
## GIF Introduction:

<img src="screenshot/showgif.gif" width ="40%">

**<font color=red>First, you need to use "DraweeView" as the carrier</font>**

**<font color=red>Second, native "Image" do not support GIF</font>**

1.How to load a GIF by "Glide"

a. load gif from network

```
    DraweeView image = (DraweeView) findComponentById(ResourceTable.Id_image);
    Glide.with(this)
         .asGif()
         .load("https://img.blog.csdnimg.cn/2019120901303086.gif);
         .into(image);
```

b. load gif from native.
```
         DraweeView image = (DraweeView) findComponentById(ResourceTable.Id_drawview);
            Glide.with(this)
                 .asGif()
                 .load(ResourceTable.Media_happygirl);
                 .into(draweeView);  
```

2. Lifecycle Management. When you don't need to show the gif, you need call `draweeView.stopGif();`

 ```
    DraweeView draweeView;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_dodo1_slice_layout);
        draweeView = (DraweeView) findComponentById(ResourceTable.Id_draweeView);
        Glide.with(getApplicationContext())
           .asGif()
           .load(ResourceTable.Media_happygirl);
           .into(draweeView); 
        
    }     

    @Override
    protected void onBackground() {
        super.onBackground();
        draweeView.stopGif();
    }
 ```

3. setScaleType Function

a. support Scale Mode

```
    public static enum ScaleMode {
        ZOOM_CENTER,
        ZOOM_START,
        ZOOM_END,
        STRECH,
        CENTER,
        INSIDE,
        CLIP_CENTER;
    }
```

b. how to use

```
    draweeView.setScaleType(mode);
```

4. **Important Note:**
    1. **Currently the DraweeView must be used to use the GIF.** 
    2. **If a context with a long lifecycle of a Glide is used, for example, applicationContext, call the stopGif method of the DraweeView when the GIF page ends to stop the Glide to reduceresource waste.**
    3.   **<font color=red>If you want to use the GIF capability of Glide, the native image does not support this function beacuse Image and Element are independent and cannot be redrawn by using Element.  To support GIF,  you need to customize Image. For details, see the implementation of DraweeView</font>**

## Installation instructions:

```
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
implementation 'io.openharmony.tpc.thirdlib:glide:1.1.3'

```
