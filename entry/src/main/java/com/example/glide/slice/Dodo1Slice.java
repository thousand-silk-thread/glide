/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.drawableability.DraweeView;
import com.example.glide.MyApplication;
import com.example.glide.ResourceTable;
import com.example.glide.SecondAbility;
import com.example.glide.utils.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;

/**
 * test Glide asGif() with Slice
 */
public class Dodo1Slice extends AbilitySlice {
    DraweeView draweeView;
    DraweeView image;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        MyApplication.register(this, "Dodo1Slice");
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_dodo1_slice_layout, null, false);
        super.setUIContent(rootLayout);
        image = (DraweeView) findComponentById(ResourceTable.Id_image);
        Glide.with(this)
                .asGif()
                .load("https://img-blog.csdnimg.cn/2019120901303087.gif")
                .into(image);

        draweeView = (DraweeView) findComponentById(ResourceTable.Id_draweeview);
        Glide.with(this)
                .asGif()
                .load(ResourceTable.Media_happygirl)
                .into(draweeView);

        Button nextSlice = (Button) findComponentById(ResourceTable.Id_nextSlice);
        nextSlice.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new Dodo2Slice(), new Intent());
//                terminateAbility();
            }
        });
        Button nextA = (Button) findComponentById(ResourceTable.Id_nextAbility);
        nextA.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder();
                operationBuilder.withBundleName(getContext().getBundleName())
                        .withAbilityName(SecondAbility.class);
                Operation operation = operationBuilder.build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
        Button nextScale = (Button) findComponentById(ResourceTable.Id_nextScale);
        final int[] count = {0};
        nextScale.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Image.ScaleMode[] args = Image.ScaleMode.values();
                Image.ScaleMode mode = args[count[0] % args.length];
                image.setScaleType(mode);
                draweeView.setScaleType(mode);
                count[0]++;
            }
        });
        final boolean[] stopFlag = {false};
        Button stop = (Button) findComponentById(ResourceTable.Id_stop);
        stop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (stopFlag[0]) {
                    image.startGif();
                    draweeView.startGif();
                } else {
                    image.stopGif();
                    draweeView.stopGif();
                }
                stopFlag[0] = !stopFlag[0];
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(draweeView != null) {
            draweeView.stopGif();
        }
        if(image != null) {
            image.stopGif();
        }
    }
}
