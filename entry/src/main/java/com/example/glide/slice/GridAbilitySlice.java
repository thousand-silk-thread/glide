package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.example.glide.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.dispatcher.TaskDispatcher;

public class GridAbilitySlice extends AbilitySlice {
    private TableLayout appGrid;
    private Text titleView;

    String[] imgs = new String[]{
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB1/ICON/20181123101959397.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835516.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835550.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181204201914769.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181204201914650.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835665.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190228133836742.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20200701101141753.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190228093058900.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190228093959011.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190307093744723.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20210111113000732.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20200914170939173.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190228101554720.png",
            "https://creditcard.bankcomm.com/tfimg/public00/M00/12/47/trScKF9bMMWAAjUwAAAWtnBWee4593.png",
            "https://creditcard.bankcomm.com/tfimg/public00/M00/12/47/trScKF_tmhSAR234AAKPW8DLRNU169.png",
            "https://creditcard.bankcomm.com/tfimg/public00/M00/12/47/trScKF_3_XuAcdhrAADEwFpryNM503.png",
            "https://creditcard.bankcomm.com/tfimg/public00/M00/12/47/trScJ1_2tHSAP_14AABZ6zkaXyY046.png",
            "https://creditcard.bankcomm.com/tfimg/public00/M00/12/47/trScJ1_3_GaAAzY6AAHXvU3zd8A760.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835786.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181128181455629.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835954.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835813.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835738.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835767.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181122203835900.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20190325175558735.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20190812105407545.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/yunying3/20190129173411063.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/yunying3/20190129173533483.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181204201914829.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/APP3.0/TAB2/ICON/20181204201914673.png",
            "https://creditcard.bankcomm.com/mapp/menu/img/3.0shouyeicon/20190909094627967.png"
    };

    DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(360, ComponentContainer.LayoutConfig.MATCH_CONTENT);

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_grid_view);

        appGrid = (TableLayout) findComponentById(ResourceTable.Id_grid);
        titleView = (Text) findComponentById(ResourceTable.Id_text);
        titleView.setText("glide images");

        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        appGrid.setColumnCount(3);
        appGrid.setRowCount(3);
        appGrid.verifyLayoutConfig(layoutConfig);
        prepareGridLayout();

        TaskDispatcher dispatcher = getContext().getUITaskDispatcher();
        dispatcher.asyncDispatch(new Runnable() {
            @Override
            public void run() {
                loadGlideImages();
            }
        });
    }


    private void prepareGridLayout() {
        int itemWidth = 350;
        ComponentContainer.LayoutConfig itemLayoutConfig = new ComponentContainer.LayoutConfig(itemWidth, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        itemLayoutConfig.setMarginTop(20);
        appGrid.removeAllComponents();
        for (int position = 0; position < imgs.length; position++) {
            DirectionalLayout directionalLayout = new DirectionalLayout(this);
            directionalLayout.setOrientation(Component.VERTICAL);
            directionalLayout.setAlignment(LayoutAlignment.CENTER);
            Image image = new Image(this);
            image.setLayoutConfig(layoutConfig);

            Text text = new Text(getContext());
            text.setText("Label");
            text.setTextSize(16, Text.TextSizeType.FP);
            directionalLayout.addComponent(image);
            directionalLayout.addComponent(text);
            directionalLayout.setLayoutConfig(itemLayoutConfig);
            appGrid.addComponent(directionalLayout);
        }
    }


    private void loadGlideImages() {
        for (int position = 0; position < imgs.length; position++) {
            DirectionalLayout directionalLayout = (DirectionalLayout) appGrid.getComponentAt(position);
            Image image = (Image) directionalLayout.getComponentAt(0);

            Glide.with(this).load(imgs[position])
                    .placeholder(ResourceTable.Media_icon)
                    .into(image);
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
