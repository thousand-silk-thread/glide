package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.LogUtil;
import com.example.glide.ResourceTable;
import com.example.glide.utils.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.agp.utils.DimensFloat;

public class GlideXmlAbilitySlice extends AbilitySlice {
    Image image1, image2, image3;
    private Context classcontext;
    private static String TAG ="GlideXmlAbilitySlice";
    private Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        classcontext =getContext();
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_test_popup, null, false);
        image1 = ResUtil.findViewById(rootLayout, ResourceTable.Id_image_test);
        image2 = ResUtil.findViewById(rootLayout, ResourceTable.Id_image_test2);
        image3 = ResUtil.findViewById(rootLayout, ResourceTable.Id_image_test3);



        button =ResUtil.findViewById(rootLayout, ResourceTable.Id_basic);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
// Add a listener for the click event to the control.
            public void onClick(Component Component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_jellyfish)
                        .into(image1);

                Glide.with(classcontext)
                        .load( ResourceTable.Media_tiger)
                        .into(image2);

                Glide.with(classcontext)
                        .load( ResourceTable.Media_flower)
                        .into(image3);
            }
        });


        setUIContent(rootLayout);
    }

}
