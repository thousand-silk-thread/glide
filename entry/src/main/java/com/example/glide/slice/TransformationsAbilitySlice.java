package com.example.glide.slice;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.load.resource.bitmap.Rotate;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.util.Preconditions;
import com.example.glide.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import com.bumptech.glide.Glide;
import com.example.glide.utils.LogUtil;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.RectFloat;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.media.image.ImagePacker;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.global.resource.NotExistException;
import ohos.media.common.sessioncore.*;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;
import ohos.global.resource.*;
import ohos.agp.components.element.Element;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import org.jetbrains.annotations.NotNull;

public class TransformationsAbilitySlice extends AbilitySlice {
    public Image imageview;
    public Image imageview2;
    public Image imageview3;

    // the media list got from the service
    private List<AVElement> avElementList = new ArrayList<>();
    private ComponentContainer mRootId;
    private Context classcontext;
    private static String TAG ="Glide";
    private Button button, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12;

    private ArrayList<String> data = new ArrayList<>();
    private AbilitySlice mSlice;

    DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(350, ComponentContainer.LayoutConfig.MATCH_CONTENT);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG, "onStart");
        classcontext =getContext();
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_transformations_main, null, false);

        button = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_1);
        button2 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_2);
        button3 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_3);
        button4 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_4);
        button5 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_5);
        button6 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_6);
        button7 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_7);
        button8 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_8);
        button9 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_9);
        button10 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_10);
        button11 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_11);
        button12 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_12);

        imageview = (Image)rootLayout.findComponentById(ResourceTable.Id_imageview1);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtil.info("TAS", "onclick on Button");

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .circleCrop()
                        .into(imageview);
            }
        });

        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .transform( new RoundedCorners(180))
                        .into(imageview);
            }
        });

        button3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .transform( new GranularRoundedCorners(150, 30, 80, 250))
                        .into(imageview);
            }
        });

        button4.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .centerCrop()
                        .into(imageview);
            }
        });

        button5.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .fitCenter()
                        .into(imageview);
            }
        });

        button6.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .centerInside()
                        .into(imageview);
            }
        });

        button7.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .transform( new Rotate(90))
                        .into(imageview);
            }
        });

        button8.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .placeholder(ResourceTable.Media_flower)
                        .into(imageview);
            }
        });

        button9.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Glide.with(classcontext)
                        .load( "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132")
                        .error(ResourceTable.Media_flower)
                        .into(imageview);
            }
        });

        button10.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Element element = new PixelMapElement(GlideAbilitySlice.getPixelMap(classcontext, ResourceTable.Media_photo1));
                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .placeholder(element)
                        .into(imageview);
            }
        });

        button11.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Element element = new PixelMapElement(GlideAbilitySlice.getPixelMap(classcontext, ResourceTable.Media_penguins));
                Glide.with(classcontext)
                        .load( "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132")
                        .error( element)
                        .into(imageview);
            }
        });

        button12.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Element element = new PixelMapElement(GlideAbilitySlice.getPixelMap(classcontext, ResourceTable.Media_penguins));
                Glide.with(classcontext)
                        .load( ResourceTable.Media_desert)
                        .placeholder(element)
                        .into(imageview);
            }
        });

        super.setUIContent(rootLayout);

    }
}

