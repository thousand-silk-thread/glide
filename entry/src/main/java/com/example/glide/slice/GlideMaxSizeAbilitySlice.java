package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.glide.ResourceTable;
import com.example.glide.model.IntModel;
import com.example.glide.utils.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * 首先得保证手机里面含有大小图gif图等等
 */
public class GlideMaxSizeAbilitySlice extends AbilitySlice {

    private static final String URI_HEADER = "dataability:///media/external/images/media/";
    private ListContainer mListContainer;
    private List<String> mData = new ArrayList<>();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_max_size);
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_img_list);
        findComponentById(ResourceTable.Id_btn_load_id).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getUriId();
            }
        });
        findComponentById(ResourceTable.Id_btn_load).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ImageBaseAdapter adapter = new ImageBaseAdapter(getAbility(), mData);
                mListContainer.setItemProvider(adapter);
            }
        });

        reqPermissions();
    }

    private void reqPermissions() {
        if (verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                requestPermissionsFromUser(new String[]{"ohos.permission.READ_MEDIA",
                        "ohos.permission.WRITE_MEDIA"}, 1000);
            }
        }
    }

    private void getUriId() {
        mData.clear();
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        DataAbilityPredicates predicates = new DataAbilityPredicates();
        //设置过滤条件
        //predicates.equalTo(AVStorage.Images.Media.DATA,"xxxx");
        try {
            ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                    new String[]{AVStorage.Images.Media.ID}, null);
            if (resultSet != null) {
                if (resultSet.goToFirstRow()) {
                    do {
                        long id = resultSet.getLong(resultSet.getColumnIndexForName(AVStorage.AVBaseColumns.ID));
                        StringBuilder buffer = new StringBuilder();
                        buffer.append(URI_HEADER);
                        buffer.append(id);
                        mData.add(buffer.toString());
                    } while (resultSet.goToNextRow());
                }
            }
        } catch (Exception e) {
            LogUtil.error("etUriId", "error message:" + e.getMessage());
        }
    }

    public class ImageBaseAdapter extends BaseItemProvider {

        private Context context;
        private List<String> Modeles;


        public ImageBaseAdapter(Context context, List<String> modeles) {
            this.context = context;
            this.Modeles = modeles;
        }

        @Override
        public int getCount() {
            return Modeles.size();
        }

        @Override
        public Object getItem(int i) {
            return Modeles.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            String model = Modeles.get(i);
            Component container = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_max_img_list_item, null, false);
            Image itemAvatar = (Image) container.findComponentById(ResourceTable.Id_item_image);
            refreshImageView(itemAvatar, model);
            return container;
        }

        private void refreshImageView(Image aiv, String url) {
            if (aiv == null) {
                return;
            }
            Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(new RequestOptions()
                            .placeholder(ResourceTable.Media_icon)
                            .error(ResourceTable.Media_icon)
                            .centerCrop())
                    .into(aiv);
        }
    }
}
