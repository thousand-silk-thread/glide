package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.example.glide.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class GlideScaleModeSlice extends AbilitySlice {
    private static final String TAG = GlideScaleModeSlice.class.getSimpleName();
    private long count = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_glide_scalemode_slice_layout, null, false);
        super.setUIContent(rootLayout);

        Image image1 = (Image) findComponentById(ResourceTable.Id_image1);
        Image image2 = (Image) findComponentById(ResourceTable.Id_image2);
        Image image3 = (Image) findComponentById(ResourceTable.Id_image3);

        Glide.with(this).load(ResourceTable.Media_tiger).into(image1);
        Glide.with(this).load(ResourceTable.Media_tiger).into(image2);
        Glide.with(this).load(ResourceTable.Media_tiger).into(image3);

        final Image.ScaleMode[] scaleModes = Image.ScaleMode.values();

        Button change = (Button) findComponentById(ResourceTable.Id_change);
        change.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Image.ScaleMode scaleModes1 = scaleModes[(int) (count% scaleModes.length)];

                change.setText("ScaleMode:"+ scaleModes1.name());

                image1.setScaleMode(scaleModes1);
                image2.setScaleMode(scaleModes1);
                image3.setScaleMode(scaleModes1);

                Glide.with(GlideScaleModeSlice.this).load(ResourceTable.Media_tiger).into(image1);
                Glide.with(GlideScaleModeSlice.this).load(ResourceTable.Media_tiger).into(image2);
                Glide.with(GlideScaleModeSlice.this).load(ResourceTable.Media_tiger).into(image3);

                count++;
            }
        });

    }
}
