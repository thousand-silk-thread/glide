package com.example.glide.utils;

public class Const {
    /** Constant for one minute */
    public static final long MINUTE = 60 * 1000L;

    /** the half value */
    public static final float FLOAT_HALF = 0.5f;

    public static final String KEY_DEFAULT = "default";
    public static final String KEY_INITIAL_PAGE = "initial_page";
    public static final String KEY_VIA_METHODS = "via_methods";
    public static final String KEY_VIA_LAYOUT = "via_layout";
    public static final String KEY_VIA_THEMES = "via_themes";
    public static final String KEY_WITH_LISTENER = "with_listener";
    public static final String KEY_CENTER_CLICK_LISTENER = "center_click_listener";
    public static final String KEY_BOTTOM = "bottom";
    public static final String KEY_TRIANGLE="triangle";
    public static final String KEY_NO_FADE="no_fade";

    private Const() {
    }
}

