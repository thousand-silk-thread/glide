/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.glide;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.drawableability.DraweeView;
import com.example.glide.slice.Dodo2Slice;
import com.example.glide.slice.Dodo3Slice;
import com.example.glide.slice.MainAbilitySlice;
import com.example.glide.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.security.SystemPermission;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.NotNull;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * test Glide asGif() with Ability
 */
public class SecondAbility extends Ability {
    private static String TAG = "Glide";
    private ArrayList<String> data = new ArrayList<>();
    private DataAbilityHelper helper;
    private Context classcontext;
    DraweeView draweeView;
    DraweeView image;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        MyApplication.register(this, "SecondAbility");
        setUIContent(ResourceTable.Layout_dodo1_slice_layout);

        image = (DraweeView) findComponentById(ResourceTable.Id_image);
        Glide.with(this)
                .asGif()
                .load(ResourceTable.Media_music)
                .into(image);

        draweeView = (DraweeView) findComponentById(ResourceTable.Id_draweeview);

        Glide.with(this)
                .asGif()
                .load(ResourceTable.Media_happygirl)
                .into(draweeView);


        Button nextSlice = (Button) findComponentById(ResourceTable.Id_nextSlice);
        nextSlice.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        Button nextA = (Button) findComponentById(ResourceTable.Id_nextAbility);
        nextA.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder();
                operationBuilder.withBundleName(getContext().getBundleName()) // 这个必须填包名
                        .withAbilityName(SecondAbility.class); // 这个必须填包名+类名
                Operation operation = operationBuilder.build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
        Button nextScale = (Button) findComponentById(ResourceTable.Id_nextScale);
        final int[] count = {0};
        nextScale.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Image.ScaleMode[] args = Image.ScaleMode.values();
                Image.ScaleMode mode = args[count[0] % args.length];
                // image.setScaleType(mode);
                draweeView.setScaleType(mode);
                image.setScaleType(mode);
                count[0]++;
            }
        });
        final boolean[] stopFlag = {false};
        Button stop = (Button) findComponentById(ResourceTable.Id_stop);
        stop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (stopFlag[0]) {
                    image.startGif();
                    draweeView.startGif();
                } else {
                    image.stopGif();
                    draweeView.stopGif();
                }
                stopFlag[0] = !stopFlag[0];
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(image != null) {
            image.stopGif();
        }
        if(draweeView != null) {
            draweeView.stopGif();
        }
    }
}
