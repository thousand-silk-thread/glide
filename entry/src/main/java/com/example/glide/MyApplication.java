package com.example.glide;

import com.example.glide.utils.LogUtil;
import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.ability.ILifecycle;
import ohos.aafwk.ability.Lifecycle;
import ohos.aafwk.ability.LifecycleStateObserver;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    /**
     * add log
     *
     * @param context ability or slice
     * @param tag     log's tag
     */
    public static void register(Context context, String tag) {
        if (context instanceof ILifecycle) {
            Lifecycle lifecycle = ((ILifecycle) context).getLifecycle();
            lifecycle.addObserver(new LifecycleStateObserver() {
                @Override
                public void onStateChanged(Lifecycle.Event event, Intent intent) {
                    LogUtil.error("lifecycle", "tag =" + tag + " event =" + event.name());
                }
            });
        }
    }
}
