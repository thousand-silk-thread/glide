#Glide

### 1. Add GIF Ability

### (1) module.gif_decoder fixed StandardGifDecoder.java

Reads frame data from a GIF image source and decodes it into indvidual frames per animation purposes. Image data can be read from either and Inputstream source or byte[].

### (2) module:library create path: com.bumptech.glide=>load=>resource=>gif

##### Our goal at this stage is to parse and show.

##### 1. Constructing the gif parsing system.

a. fixed All files in the gif folder.

b. Glide.java registry support BUCKET_GIF

c. RequestManager.java add asGif() method.

d. So far, we can obtain the frame of the Gif.

##### 2. Build a user-defined image and then support element drawing.

a. module:library create path: com.bumptech.glide=>resource=>gif=>drawableability

b. Create RootShapeElement.java support callback

c. Create DraweeView.java support draw canvas by Element;

** (The overall process is as follows: **

1. **"Glide.with(this).asGif().load(resId).into(draweeview)" => After the loading is successful, the "DrawableImageViewTarget.java" assigns the "Elemet" value to the target "Image". (DraweeView)"**

 ** set "animatable", execute "animatable.onstart()" to enter "GifDrawable.onStart()", start to obtain "Frame" of the first frame, and "GifFrameLoader" starts the loop. The callback of the second frame is "GifDrawable.onFrameReady()".**

2. **"GifDrawable" invokes "invalidateSelf" after the "onFrameReady" method, and then searches for the control that has registered "Callback" to execute the invalidateDrawable" method.**

**"DrawableView" triggers "drawToCanvas" after "invalidatedrawable" is invoked,**
**To support ScaleType, an empty PixelMap is create, and the GifDrawable is used to dra the Gif content. Finally, the DraweeView performs transformation and displays the final effect.)**

### 2. Add AbilitySlice and Ability LifeCycle management for Glide.

** The performance consumption of GIF loading is huge, so we should create the GIF loading at the right time. Therfore, slice and mobility life cycle managements added. When the "Glide.with(Ability or AbilitySlice)" is used, we obtain the ILifeCyle and monitor its lifecycle. When the ILifeCycle enters the BACKGround state, we actively stop the GIF work and start the GIF work when the ILifeCycle enters the INACTIVE state.**

** The specific task is to use RequestManagerFragment to bind the ActivityFragmentLifeCycle. Then RequestManagerFragment obtains the life cycle of the ActivitySlice and listens to the lifecycle.**

### 3. How to load a GIF by "Glide"

a. load gif from network

```
java
     DraweeView image = (DraweeView) findComponentById(ResourceTable.Id_image);
        Glide.with(this)
             .asGif()
             .load("https://img.blog.csdnimg.cn/2019120901303086.gif);
             .into(image);
```

b. load gif from native.

```
java
    DraweeView image = (DraweeView) findComponentById(ResourceTable.Id_drawview);
                Glide.with(this)
                     .asGif()
                     .load(ResourceTable.Media_happygirl);
                     .into(draweeView);  
    
```

<font color=red>**Important Note:**</font>

1. **<font color=red>Currently the DraweeView must be used to use the GIF. </font>**
2. **<font color=red>If a context with a long lifecycle of a Glide is used, for example, applicationContext, call the stopGif method of the DraweeView when the GIF page ends to stop the Glide to reduceresource waste.</font>**
3. **<font color=red>If you want to use the GIF capability of Glide, the native image does not support this function beacuse Image and Element are independent and cannot be redrawn by using Element.  To support GIF,  you need to customize Image. For details, see the implementation of DraweeView</font>**
